import React from 'react'
import ReactDOM from 'react-dom'

import App from '../components/layout/App'

const app = document.getElementById('app')

ReactDOM.render(<App />, app)