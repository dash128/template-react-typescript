import React, { Component, Fragment } from 'react'

import Button from '@material-ui/core/Button'

class App extends Component {

    render() {
        return (
            <Fragment>
                <h1>App React con TS </h1>
                <Button variant="contained" color="primary">
                    Button Material
                </Button>
            </Fragment>

        )
    }
}

export default App